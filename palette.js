
/**
 * @todo There's a lot of requests in this file that could be async
 * but currently aren't due to race conditions. We could use jQuery's
 * ajax queue plugin to regain some snappiness.
 */

function palette(id, sections) {
	this.id = id;
	this.selector = '#' + id;
	this.sections = new Array();
	
	var sections = jQuery(this.selector).find('.palette-section');
	
	for(i=0; i<sections.length; i++) {
		this.sections.push(jQuery(sections[i]).attr('id'));
	}
	
	if(Drupal.settings.palette[this.id] && Drupal.settings.palette[this.id].coordinates) {
		
		var position = Drupal.settings.palette[this.id].coordinates.split(',');
		
		// Put the palette where it was last left.
		jQuery(this.selector).css({
			'top'  : checkPosition(position[1], 'y') + 'px',
			'left' : checkPosition(position[0], 'x') + 'px'
		});
	}
	
	// Make it draggable
	jQuery(this.selector).draggable({
		handle: 'p',
		containment: 'document',
		
		stop: function(event, ui) {
			var paletteID = jQuery(this).closest('.palette').attr('id');
			var palette = window[paletteID];
			
			var suggestedTop = checkPosition(ui.position.top, 'y');
			var suggestedLeft = checkPosition(ui.position.left, 'x');
			
			// Stop people putting the palette off the screen
			if(suggestedLeft != ui.position.left) {
				jQuery(palette.selector).css({ 'left': suggestedLeft + 'px'});
			}
			if(suggestedTop != ui.position.top) {
				jQuery(palette.selector).css({ 'top': suggestedTop + 'px'});
			}
			
			palettePosition(palette.id, suggestedLeft, suggestedTop);
		}
	});
	
	// Make the close button work
	jQuery(this.selector).find("a.close").click(function () {
		var palette = jQuery(this).closest('.palette').attr('id');
		window[palette].hide();
		jQuery("li."+palette+" a").removeClass("selected");
	});
	
	// Make the min/max button work
	jQuery(this.selector).find("a.minimise").click(function () {
		var palette = jQuery(this).closest('.palette').attr('id');
		window[palette].toggleMinimised();
	});
	
	if(Drupal.settings.palette[this.id] && (Drupal.settings.palette[this.id].maximised == 0)) {
		this.minimise();
	}
	
	// Make the section headers clickable
	for(var i in this.sections) {
		jQuery('#'+this.sections[i]+' h3').click(function () {
			var sectionName = jQuery(this).closest('.palette-section').attr('id');
			var palette = jQuery(this).closest('.palette').attr('id');
			return window[palette].showSection(sectionName);
		});
	}
	
	// Make the window raise when clicked
	jQuery(this.selector).mousedown(function () {
		var palette = jQuery(this).closest('.palette').attr('id');
		window[palette].focus();
	});
	
	// Set the selected section to open.
	var default_section = null;
	
	// Is there a currently open one in settings?
	for(var i in this.sections) {
		if(this.sections[i] == Drupal.settings.palette[this.id].selected_section) {
			default_section = this.sections[i];
			break;
		}
	}
	
	// If not, fall back to the first one:
	if(default_section == null) {
		default_section = this.sections[0];
	}
	
	// Show the active section
	this.showSection(default_section);
	
	// Fade the palettes in. They're hidden in the CSS by default.
	// (This stops them jumping about while the code above runs)
	jQuery(this.selector).fadeIn("slow");
}

palette.prototype.focus = function () {
	jQuery("div.palette").removeClass("dragselected");
	jQuery(this.selector).addClass("dragselected");
};

palette.prototype.show = function (rightnow) {
	
	if(rightnow) {
		jQuery(this.selector).show();
	}
	else {
		jQuery(this.selector).fadeIn("slow");
	}
	
	if(Drupal.settings.palette[this.id] && !Drupal.settings.palette[this.id].visible) {
		jQuery.ajax({
			url: '/admin/ajax/palettes/' + this.id +'/visible/1',
			dataType: 'html',
			async: false
		});
		Drupal.settings.palette[this.id].visible = 1;
	}
};

palette.prototype.hide = function () {
	jQuery(this.selector).fadeOut("fast");
	jQuery(this.selector).remove();
	
	if(Drupal.settings.palette[this.id] && Drupal.settings.palette[this.id].visible) {
		jQuery.ajax({
			url: '/admin/ajax/palettes/' + this.id +'/visible/0',
			dataType: 'html',
			async: false // False on purpose, so we don't break a reload
		});
		Drupal.settings.palette[this.id].visible = 0;
	}
};

palette.prototype.toggleMinimised = function () {
	
	if(Drupal.settings.palette[this.id] && Drupal.settings.palette[this.id].maximised) {
		this.minimise();
	}
	else {
		this.maximise();
	}
};

palette.prototype.maximise = function () {
	
	jQuery(this.selector).find("div.palette-content").slideDown();
	jQuery(this.selector).find("a.maximise").removeClass('maximise').addClass('minimise');
	
	if(Drupal.settings.palette[this.id].maximised != 1) {
		Drupal.settings.palette[this.id].maximised = 1;
		jQuery.ajax({
			url: '/admin/ajax/palettes/' + this.id +'/maximised/1',
			async: true
		});
	}
};

palette.prototype.minimise = function () {
	
	if(jQuery(this.selector).is(':visible')) {
		jQuery(this.selector).find("div.palette-content").slideUp();
	}
	else {
		jQuery(this.selector).find("div.palette-content").hide();
	}
	jQuery(this.selector).find("a.minimise").removeClass('minimise').addClass('maximise');
	
	if(Drupal.settings.palette[this.id].maximised != 0) {
		Drupal.settings.palette[this.id].maximised = 0;
		jQuery.ajax({
			url: '/admin/ajax/palettes/' + this.id +'/maximised/0',
			async: true
		});
	}
};

palette.prototype.showSection = function (section) {
	
	// 2 modes - one with nice animations.
	// One that works when the palette's hidden.
	if(jQuery(this.selector).is(':visible')) {
		
		// Palette in visible mode
		for(var i in this.sections) {
			
			var selector = this.selector +" #" + this.sections[i];
			
			if(section != this.sections[i] && jQuery(selector + " .palette-section-content").is(':visible')) {
				jQuery(selector + " h3").removeClass("active");
				jQuery(selector + " .palette-section-content").slideUp("fast");
			}
		}
	}
	else {
		// Palette is hidden mode.
		for(var i in this.sections) {
			
			var selector = this.selector +" #" + this.sections[i];
			
			if(section != this.sections[i]) {
				jQuery(selector + " hc").removeClass("active");
				jQuery(selector + " .palette-section-content").hide();
			}
		}
	}
	
	jQuery(this.selector +" #" + section + " h3").addClass("active");
	jQuery(this.selector +" #" + section + " .palette-section-content").slideDown("fast");
	
	if(Drupal.settings.palette[this.id].selected_section != section) {
		jQuery.ajax({
			url: '/admin/ajax/palettes/' + this.id +'/selected_section/' + section,
			dataType: 'html',
			async: true
		});
		Drupal.settings.palette[this.id].selected_section = section;
	}
	
	return false;
};

function palettePosition(name, x, y) {
	jQuery.ajax({
		url: '/admin/ajax/palettes/' + name +'/coordinates/' + x + ',' + y,
		dataType: 'html',
		async: true
	});
}

function checkPosition(pos, axis) {
	
	// Top & left
	if(pos == null || pos < 10) {
		return 10;
	}
	
	var winWidth = jQuery(window).width();
	
	// Right
	if((axis == 'x') && (pos > winWidth)) {
		return winWidth - 30;
	}
	
	var winHeight = jQuery(window).height();
	
	// Bottom
	if((axis == 'y') && (pos > winHeight)) {
		return winHeight - 30;
	}
	
	return pos;
}

(function ($) {
	Drupal.behaviors.palette = {
		attach: function (context, settings) {
			// Get each palette from the markup that isn't set up
			$('.palette').not('.ui-draggable').each(function (index, palette_element) {
				
				var id = $(palette_element).attr('id');
				window[id] = new palette(id);
			});
		}
	};
}(jQuery));
