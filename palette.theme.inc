<?php

/**
 * Theme function for theming a palette
 * @param $args
 */

function theme_palette($args) {

  $id = $args['id'];
  $title = $args['title'];
  $sections = $args['sections'];

  ob_start();
  echo "<div id=\"{$id}\" class=\"palette\">";
  echo "<p class=\"dragme\">{$title}<a class=\"minimise\" href=\"#\" alt=\"Minimise this palette\">Minimise</a><a class=\"close\" href=\"#\" alt=\"Close this palette\">Close</a></p>";
  echo '<div class="palette-content">';
  foreach ($sections as $section) {
    echo theme('palette_section', $section);
  }
  echo '</div>';
  echo '</div>';
  return ob_get_clean();
}

/**
 * Theme function for theming a palette section
 * @param $section
 */

function theme_palette_section($section) {

  if (!isset($section['content'])) {
    $section['content'] = '';
  }

  $content_rendered = drupal_render($section['content']);

  return <<<HTML
  <div id="{$section['id']}" class="palette-section clearfix">
    <h3>{$section['title']}</h3>
    <div class="palette-section-content">{$content_rendered}</div>
  </div>
HTML;
}